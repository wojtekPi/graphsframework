package configuration;

import java.util.logging.Logger;

import controller.ActualState;

public class ApplicationRuntimeException extends RuntimeException {

	private final static Logger LOGGER = Logger.getLogger(ApplicationRuntimeException.class.getName());
	
	public ApplicationRuntimeException(){
		super();
	}
	
	public ApplicationRuntimeException(String message){
		super("Error in GraphsFramework application. : " + message);
		LOGGER.severe("Error in GraphsFramework application. : " + message + ". Exception is throwed.");
	}
}

package configuration;

import java.io.File;

public class Configurator {

	private static String APPLICATION_DIR_NAME = ".GraphsFramework";
	private static File APPLICATION_DIR;
	private static String LOGS_DIR_NAME = "logs";
	private static File LOGS_DIR;
	
	public static void init(){
		
		
	}
	
	public static void preInit(){
		createDirForApplictionData();
		createDirForLogs();
	}


	public static void createDirForApplictionData(){
		APPLICATION_DIR = new File(System.getProperty("user.home")+File.separator+APPLICATION_DIR_NAME);
		try {
			APPLICATION_DIR.mkdirs();
		} catch(Exception e) {
			System.out.println("Aplication dir: "+ APPLICATION_DIR.getAbsolutePath() +" couldn't be created.");
			e.printStackTrace();
		}
	}
	
	private static void createDirForLogs() {
		LOGS_DIR = new File(APPLICATION_DIR, LOGS_DIR_NAME);
		try {
			LOGS_DIR.mkdirs();
		} catch(Exception e) {
			System.out.println("Logs dir: "+ LOGS_DIR.getAbsolutePath() +" couldn't be created.");
			e.printStackTrace();
		}
	
	}
	public static File getLogsDir(){
		return LOGS_DIR;
		
	}
	
	
}

package configuration.logs;

import java.io.File;
import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import configuration.Configurator;

public class LoggingConfigurator {

	private final static Logger LOGGER = Logger.getLogger(LoggingConfigurator.class.getName());
	private final static Logger GLOBAL_LOGGER = LogManager.getLogManager().getLogger("");
	private final static String LOG_FILE_NAME = "log.txt";
	private static File LOG_FILE;
	static private FileHandler fileTxt;

	
	public static void configureLogging(){
		GLOBAL_LOGGER.setLevel(Level.INFO);
		setFileToSaveLogs();
		setHandlers();
		LOGGER.info("Logger is enabled.");
	}

	private static void setFileToSaveLogs() {
		LOG_FILE = new File(Configurator.getLogsDir(), LOG_FILE_NAME);
		System.out.println("Logs saved in file: " + LOG_FILE.getAbsolutePath());
	}

	private static void setHandlers() {
		try {
			fileTxt = new FileHandler(LOG_FILE.getAbsolutePath());
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		MyLogFormatter myLogFormater = new MyLogFormatter();
		fileTxt.setFormatter(myLogFormater);
		GLOBAL_LOGGER.addHandler(fileTxt);
		GLOBAL_LOGGER.info("File handler configured.");

		
	}
}

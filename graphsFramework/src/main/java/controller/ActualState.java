package controller;

import java.util.logging.Logger;

import javax.swing.JPanel;

import view.mainViewPanel.GraphTab;
import controller.GraphsManagers.AbstractGraphManager;
import controller.Strategies.Strategy;

public class ActualState {

	private final static Logger LOGGER = Logger.getLogger(ActualState.class.getName());
	
	public static GraphTab getActiveTab() {
		return MainViewManager.getActualTab();
	}

	public static AbstractGraphManager getActiveGraphManager() {
		AbstractGraphManager graphManager = GraphsRegistryManager.getGraphManager(getActiveTab().getLabel());
		return graphManager;
	}
	
	public static Strategy getActualStrategy(){
		return StrategyManager.getStrategy();
	}
	
}

package controller;

import java.awt.EventQueue;
import java.util.logging.Logger;

import configuration.Configurator;
import configuration.logs.LoggingConfigurator;
import view.MainWindow;

public class ApplicationStarter {
	private final static Logger LOGGER = Logger.getLogger(ApplicationStarter.class.getName());
	
	public ApplicationStarter(){
		
	}
	
	public static void start(String[] args){
		Configurator.preInit();
		configureLogger();
		LOGGER.info("Program starting.");
		showApplicationWindow();
		LOGGER.info("After showing Application window.");
	}

	private static void configureLogger() {
		LoggingConfigurator.configureLogging();
	}

	private static void showApplicationWindow() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainWindow window = new MainWindow();
					window.show();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
	}
}

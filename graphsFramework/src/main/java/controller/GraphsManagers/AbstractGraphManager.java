package controller.GraphsManagers;

import java.awt.Point;
import java.util.Set;
import java.util.logging.Logger;

import configuration.ApplicationRuntimeException;
import model.Graph;
import model.Vertex;

public abstract class AbstractGraphManager{

	private final static Logger LOGGER = Logger.getLogger(WeightedGraphManager.class.getName());
	protected String label;

	
	public AbstractGraphManager(String label){
		setLabel(label);
	}
	
	public void setLabel(String label){
		this.label = label;
	}


	public void addVertex(Point point) {
		// TODO Auto-generated method stub
		
	}
	
	public Set<? extends Vertex> getAllVertices(){
		throw new ApplicationRuntimeException("This method must be overriden!");
	}
	
}

package controller.GraphsManagers;

import model.Edge;
import model.Graph;
import model.Vertex;

public class GraphManager extends AbstractGraphManager {


	private Graph<Vertex,Edge> graph;
	
	public GraphManager(String label){
		super(label);
		createGraph(label);
	}

	private void createGraph(String label){
		graph = new Graph<Vertex,Edge>(label); 
	}
	
}

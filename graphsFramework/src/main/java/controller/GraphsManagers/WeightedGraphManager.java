package controller.GraphsManagers;

import java.awt.Point;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import controller.ActualState;
import controller.MainViewManager;
import controller.Strategies.VertexModeStartegy;
import model.WeightedEdge;
import model.WeightedGraph;
import model.WeightedVertex;

public class WeightedGraphManager extends AbstractGraphManager {
	
	private final static Logger LOGGER = Logger.getLogger(WeightedGraphManager.class.getName());
	
	private static String LABEL = "Label";
	private static String WEIGHT = "Weight";

	WeightedGraph<WeightedVertex,WeightedEdge> graph;
	
	public WeightedGraphManager(String label){
		super(label);
		createWeightedGraph(label);
	}

	private void createWeightedGraph(String label) {
		graph = new WeightedGraph<WeightedVertex, WeightedEdge>(label);
	}
	
	public void addVertex(Point coordinates){
		LinkedHashMap<String,Class<?>> requiredParams = new LinkedHashMap<String, Class<?>>();
		requiredParams.put(LABEL, String.class);
		requiredParams.put(WEIGHT, Number.class);
		Map<String, String> values = MainViewManager.askForParams(requiredParams);
		WeightedVertex vertex = new WeightedVertex(values.get(LABEL), coordinates, Float.valueOf(values.get(WEIGHT)));
		graph.addVertex(vertex);
		ActualState.getActiveTab().repaint();
		ActualState.getActiveTab().revalidate();
		
	}
	public Set<WeightedVertex> getAllVertices(){
		return graph.getAllVertices();
	}
	
}

package controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import configuration.ApplicationRuntimeException;
import controller.GraphsManagers.AbstractGraphManager;
import controller.GraphsManagers.GraphManager;
import controller.GraphsManagers.WeightedGraphManager;
import model.Edge;
import model.Graph;
import model.Vertex;
import model.WeightedEdge;
import model.WeightedGraph;
import model.WeightedVertex;

public class GraphsRegistryManager {

	private final static Logger LOGGER = Logger.getLogger(GraphsRegistryManager.class.getName());
	private static Map<String,AbstractGraphManager> graphsManagers = new HashMap<String,AbstractGraphManager>();
	

	
	public static void createWeightedGraph(String label){
		if(isGraphRegisteredBefore(label)){
			return;
		}
		else {
			WeightedGraphManager weightedGraphManager = new WeightedGraphManager(label);
			graphsManagers.put(label, weightedGraphManager);
			LOGGER.info("Graph " + label + " is registered.");
		}
	}
	
	
	public static boolean isGraphRegisteredBefore(String label){
		boolean isCreatedBefore = graphsManagers.containsKey(label);
		 if (isCreatedBefore)
			 LOGGER.warning("Graph "+label + " alredy exist!");
		 else {
			 LOGGER.info("Graph "+label + " doesn't exist.");
		 }
		 return isCreatedBefore;
	}


	public static AbstractGraphManager getGraphManager(String label) {
		if(!graphsManagers.containsKey(label)){
			throw new ApplicationRuntimeException("Graph with label " + label + " doesn't exist.");
		}
		else{
		return graphsManagers.get(label);	
		}
	}

	
	
}

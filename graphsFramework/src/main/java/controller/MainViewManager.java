package controller;

import java.util.Map;

import javax.swing.JPanel;

import view.mainViewPanel.ParametersAsker;
import view.mainViewPanel.GraphTab;
import view.mainViewPanel.MainViewPanel;

public class MainViewManager {
	
	private static MainViewPanel mainViewPanel;
	
	public static void setMainViewPanel(MainViewPanel mainViewPanelparam){
		mainViewPanel = mainViewPanelparam;
	}
	
	public static void createTabForNewGraph(String label){
		MainViewPanel.addNewTab(label);
	}
	
	public static GraphTab getActualTab(){
		return MainViewPanel.getActiveTab();
	}
	
	public static Map<String,String> askForParams(Map <String,Class<?>> params){
		ParametersAsker asker = new ParametersAsker(params);
		
		return asker.getValues();
	}
	

}

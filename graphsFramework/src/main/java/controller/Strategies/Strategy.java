package controller.Strategies;

import java.awt.event.MouseEvent;

public interface Strategy {

	public void doubleClickOnActualTabAction(MouseEvent e);

	public void singleClickOnActualTabAction(MouseEvent e);
}

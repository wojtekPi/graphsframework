package controller.Strategies;

import java.awt.event.MouseEvent;
import java.util.logging.Logger;

import controller.ActualState;
import controller.MainViewManager;
import view.mainViewPanel.GraphTab;

public class VertexModeStartegy implements Strategy {
	
	private final static Logger LOGGER = Logger.getLogger(VertexModeStartegy.class.getName());
	
	public void singleClickAction(){
		
	}
	
	public void doubleClickOnActualTabAction(MouseEvent e){
		ActualState.getActiveGraphManager().addVertex(e.getPoint());
	}

	public void singleClickOnActualTabAction(MouseEvent e) {
		
	}



}

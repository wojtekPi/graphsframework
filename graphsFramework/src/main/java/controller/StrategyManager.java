package controller;

import java.util.logging.Level;
import java.util.logging.Logger;

import view.optionsPanel.ModeButtonsPanel;
import controller.Strategies.Strategy;
import controller.Strategies.VertexModeStartegy;

public class StrategyManager {
	private final static Logger LOGGER = Logger.getLogger(StrategyManager.class.getName());

	private static Strategy actualStrategy;
	
	public static void setStrategy(Strategy strategy){
		actualStrategy = strategy;
	}
	
	public static Strategy getStrategy(){
		return actualStrategy;
	}
	
	public static void setStrategy(String strategyName){
		
		Strategy strategy = null;
		if(strategyName.equals(ModeButtonsPanel.VERTEX_BUTTON_LABEL))
			strategy = new VertexModeStartegy();
		
		if(strategyName.equals(ModeButtonsPanel.EDGE_BUTTON_LABEL))
			strategy = new VertexModeStartegy();
		
		if(strategyName.equals(ModeButtonsPanel.EDIT_BUTTON_LABEL))
			strategy = new VertexModeStartegy();
		
		
		if(strategy == null) {
			LOGGER.log(Level.WARNING,"Error in selecting strategy!");
			throw new RuntimeException();
		}
		
		setStrategy(strategy);
	}
}

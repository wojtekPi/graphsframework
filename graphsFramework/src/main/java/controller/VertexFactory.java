package controller;

import java.awt.Point;
import java.util.HashMap;
import java.util.Map;
import model.Vertex;
import model.exceptions.VertexAlredyExistException;
import model.exceptions.VertexNotFoundException;

public class VertexFactory {

	private static Map<String, Vertex> itemsRegistry = new HashMap<String,Vertex>();
	
	public static Vertex createVertex(String label, Point coordinates){
		if (isVertexAlredyCreated(label)) {
			throw new VertexAlredyExistException(getVertex(label));
		}
		Vertex newVertex = new Vertex(label,coordinates);
		itemsRegistry.put(label, newVertex);
		return newVertex;
	}
	
	public static Vertex getVertex(String label){
		if (!isVertexAlredyCreated(label)) {
			throw new VertexNotFoundException(label);
		}
		return itemsRegistry.get(label);
	}
	
	public static void removeVertex(String label){
		if (!isVertexAlredyCreated(label)) {
			throw new VertexNotFoundException(label);
		}
		itemsRegistry.remove(label);
	}
	
	public static void removeVertex(Vertex vertex){
		if (!isVertexAlredyCreated(vertex.getLabel())) {
			throw new VertexNotFoundException(vertex.getLabel());
		}
		itemsRegistry.remove(vertex.getLabel());
	}
	
	public static boolean isVertexAlredyCreated(String label){
		return itemsRegistry.containsKey(label);
		
	}
	
	public Map<String, Vertex> getItemsRegistry(){
		return itemsRegistry;
	}
}

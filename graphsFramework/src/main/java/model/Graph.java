package model;

import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Set;
import java.util.logging.Logger;

import controller.GraphsManagers.WeightedGraphManager;
import model.exceptions.VertexIsAlredyInGraph;

public class Graph<V extends Vertex,E extends Edge> extends LabeledElement {
	
	private final static Logger LOGGER = Logger.getLogger(WeightedGraphManager.class.getName());
	protected Set<V> vertices = new HashSet<V>();
	protected Set<Edge> edges = new HashSet<Edge>();
	
	public Graph(String label){
		super(label);
	}
	
	public void addVertex(V vertex){
		boolean isAdded = vertices.add(vertex);
		if (!isAdded) 
			//throw new VertexIsAlredyInGraph(vertex);
			LOGGER.warning(vertex + " alredy exist.");
	}
	
	public void removeVertex(V vertex){
		vertices.remove(vertex);
	}
	
	public void addEdge(E edge){
		edges.add(edge);
	}
	
	public void removeEdge(E edge){
		edges.remove(edge);
	}
	
	public Set<V> getAllVertices(){
		return vertices;
	}
	


}

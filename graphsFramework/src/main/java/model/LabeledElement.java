package model;

public class LabeledElement {

	private String label;
	
	public LabeledElement(String label){
		setLabel(label);
	}
	
	public void setLabel(String label) {
		this.label = label==null?"":label;
	}

	public String getLabel() {
		return label;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((label == null) ? 0 : label.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof LabeledElement))
			return false;
		LabeledElement other = (LabeledElement) obj;
		if (label == null) {
			if (other.label != null)
				return false;
		} else if (!label.equals(other.label))
			return false;
		return true;
	}

}
package model;

import java.awt.Point;
import java.util.LinkedHashMap;

public class Vertex extends LabeledElement {
	
	protected static String LABEL = "label";
	private Point coordinates;
	
	private Vertex(String label){
		super(label);
	}
	
	public Vertex(String label, Point coordinates){
		super(label);
		this.coordinates = coordinates;
	}
	
	public void setCoordinates(Point point){
		coordinates = point;
	}
	
	public Point getCoordiates(){
		return coordinates;
	}
	
	public LinkedHashMap<String,String> getInformationMap(){
		LinkedHashMap<String, String> result = new LinkedHashMap<String,String>();
		result.put(LABEL, getLabel());
		return result;
		
	}

	@Override
	public String toString(){
		String message = "Vertex: "+getLabel()+ ", with coordinates: "+coordinates + " .";
		return message;
		
	}
	
	
}

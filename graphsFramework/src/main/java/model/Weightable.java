package model;

public interface Weightable<V extends WeightedVertex,E extends WeightedEdge> {
	
	public void setVertexWeight(V vertex, Number weight);
	
	public Number getVertexWeight(V vertex);
	
	public void setEdgeWeight(E edge, Number weight);
	
	public Number getEdgeWeight(E edge);

}

package model;

public interface WeightableComponent {

	public static Number DEFAULT_WEIGHT = 1.0;
	
	public void setWeight(Number weight);
	public Number getWeight();
}

package model;

public class WeightedEdge extends Edge implements WeightableComponent {

	public static Number DEFAULT_WEIGHT = 1.0;
	private Number weight;
	
	public WeightedEdge(String label) {
		this(label, DEFAULT_WEIGHT);
	}
	
	public WeightedEdge(String label, Number weight){
		super(label);
		this.weight = weight;
	}

	public void setWeight(Number weight) {
		this.weight=weight;
		
	}

	public Number getWeight() {
		return weight;
	}

}

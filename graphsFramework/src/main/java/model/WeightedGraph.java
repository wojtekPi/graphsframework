package model;

public class WeightedGraph<V extends WeightedVertex, E extends WeightedEdge> extends Graph<V,E> implements Weightable<V,E> {

	public WeightedGraph(String label) {
		super(label);
	}

	public void setVertexWeight(V vertex, Number weight) {
		vertex.setWeight(weight);
	}

	public Number getVertexWeight(V vertex) {
		return vertex.getWeight();
	}
	
	public Number getEdgeWeight(E edge) {
		return edge.getWeight();
	}

	public void setEdgeWeight(E edge, Number weight) {
		edge.setWeight(weight);	
	}

	public Object getVertices() {
		// TODO Auto-generated method stub
		return null;
	}

}

package model;

import java.awt.Point;
import java.util.LinkedHashMap;
import java.util.logging.Logger;

import controller.GraphsManagers.WeightedGraphManager;

public class WeightedVertex extends Vertex implements WeightableComponent{
	
	private final static Logger LOGGER = Logger.getLogger(WeightedGraphManager.class.getName());
	
	protected static String WEIGHT = "weight";
	public static Number DEFAULT_WEIGHT = 1.0;
	private Number weight;

	public WeightedVertex(String label, Point coordinates) {
		this(label, coordinates, DEFAULT_WEIGHT);
	}
	
	public WeightedVertex(String label, Point coordinates, Number weight){
		super(label, coordinates);
		this.weight = weight;
		LOGGER.info("Vertex: "+ this + " created.");
		
	}

	public void setWeight(Number weight) {
		this.weight = weight;
		
	}

	public Number getWeight() {
		return weight;
	}
	
	public LinkedHashMap<String,String> getInformationMap(){
		LinkedHashMap<String, String> result = super.getInformationMap();
		result.put(WEIGHT, getWeight().toString());
		return result;
		
	}

}

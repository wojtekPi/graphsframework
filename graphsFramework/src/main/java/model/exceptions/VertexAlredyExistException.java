package model.exceptions;

import model.Vertex;

public class VertexAlredyExistException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public VertexAlredyExistException(){
		super();
	}
	
	public VertexAlredyExistException(Vertex vertex){
		super("Vertex with label: " + vertex.getLabel() + " alredy exist.");
	}
}

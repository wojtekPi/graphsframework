package model.exceptions;

import model.Vertex;

public class VertexIsAlredyInGraph extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public VertexIsAlredyInGraph(Vertex vertex){
		super("Vertex with label: " + vertex.getLabel() + " is alredy in this Graph.");
	}
}

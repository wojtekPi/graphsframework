package model.exceptions;

public class VertexNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public VertexNotFoundException(){
		super();
	}
	
	public VertexNotFoundException(String label){
		super("Vertex with label: " + label + " doesn't exist.");
	}
}

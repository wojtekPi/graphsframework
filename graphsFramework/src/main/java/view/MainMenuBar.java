package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import controller.GraphsRegistryManager;
import controller.MainViewManager;

public class MainMenuBar extends JMenuBar {
	
	private static String NEW = "New..";
	private static String GRAPH = "Graph";
	private static String WEIGHTED_GRAPH_LABEL = "Weighted graph";
	private static String WEIGHTED_GRAPH = "WeightedGraph";
	private static String ENTER_LABEL_MESSAGE = "Please, enter the label of new graph.";
	private static String ENTER_LABEL_HERE = "Eneter label here.";

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MainMenuBar(){
		initialize();
	}
	
	private void initialize(){
		addMenus();
	}
	
	private void addMenus(){
		this.add(getFileMenu());
		this.add(getNewMenu());
	}

	private JMenu getNewMenu() {
		JMenu mnNew = new JMenu(NEW);
		
		JMenuItem graph = new JMenuItem(GRAPH);
		mnNew.add(graph);
		
		JMenuItem weightedGraph = new JMenuItem(WEIGHTED_GRAPH_LABEL);
		mnNew.add(weightedGraph);
		weightedGraph.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				String graphLabel = JOptionPane.showInputDialog(ENTER_LABEL_MESSAGE  , ENTER_LABEL_HERE);
				MainViewManager.createTabForNewGraph(graphLabel);
				GraphsRegistryManager.createWeightedGraph(graphLabel);
			}
		});
		
		return mnNew;
	}

	private JMenu getFileMenu() {
		JMenu mnFile = new JMenu("File");
		
		JMenuItem mntmSave = new JMenuItem("Save");
		mnFile.add(mntmSave);
		
		JMenuItem mntmExit = new JMenuItem("Exit");
		mnFile.add(mntmExit);
		
		return mnFile;
	}
	
}

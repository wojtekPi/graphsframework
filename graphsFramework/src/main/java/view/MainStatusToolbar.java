package view;

import javax.swing.JLabel;
import javax.swing.JToolBar;

public class MainStatusToolbar extends JToolBar {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MainStatusToolbar(){
		initialize();
	}

	private void initialize() {
		add(new JLabel("Status"));
		
	}
}

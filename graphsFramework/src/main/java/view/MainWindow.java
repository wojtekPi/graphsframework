package view;

import javax.swing.JFrame;

import java.awt.BorderLayout;

import javax.swing.JToolBar;

import java.awt.Label;

import javax.swing.JMenuBar;

import controller.MainViewManager;
import controller.OptionsManager;
import controller.StatusManager;
import view.mainViewPanel.MainViewPanel;
import view.optionsPanel.OptionsPanel;

public class MainWindow {

	private JFrame frame;
	private String title = "Graphs Framework";
	private MainViewPanel mainView;
	private MainStatusToolbar toolBarStatus;
	private OptionsPanel optionsPanel;
	private JToolBar optionsToolbar;


	/**
	 * Create the application.
	 */
	public MainWindow() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		setFrameSettings();
		
		toolBarStatus = new MainStatusToolbar();
		frame.getContentPane().add(toolBarStatus, BorderLayout.SOUTH);
		StatusManager.setMainStatusToolbar(toolBarStatus);
		
		mainView = new MainViewPanel();
		frame.getContentPane().add(mainView, BorderLayout.CENTER);
		MainViewManager.setMainViewPanel(mainView);		
				
		optionsPanel = new OptionsPanel();
		optionsToolbar = new JToolBar(JToolBar.VERTICAL);
		optionsToolbar.add(optionsPanel);
		frame.getContentPane().add(optionsToolbar, BorderLayout.WEST);
		OptionsManager.setOtionsPanel(optionsPanel);
		
		JMenuBar menuBar = new MainMenuBar();
		frame.setJMenuBar(menuBar);
	}

	private void setFrameSettings() {
		frame = new JFrame();
		frame.setTitle(title);
		frame.setBounds(100, 100, 750, 500);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BorderLayout(0, 0));
	}

	public void show() {
		frame.setVisible(true);
		
	}
}

package view.mainViewPanel;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Point;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;
import java.util.logging.Logger;

import javax.swing.JPanel;

import com.sun.javafx.geom.Rectangle;

import model.Graph;
import model.Vertex;
import controller.ActualState;
import controller.ApplicationStarter;
import controller.GraphsRegistryManager;
import controller.GraphsManagers.AbstractGraphManager;

public class GraphTab extends JPanel {
	
	private final static Logger LOGGER = Logger.getLogger(GraphTab.class.getName());
	
	private Color vertexColor = Color.blue;
	private double vertexDiameter = 25.0;
	private Color popUpColor = Color.black;
	private int informationsFontSize = 10;
	private Font informationsFont = new Font(Font.SANS_SERIF, Font.PLAIN, informationsFontSize);
	private int informationMargin = 5;
	private int spaceBetweenInformations = 3;
	private double informationsPopupWidth = 80.0;


	private String label;
	
	public GraphTab(String label){
		super();
		setLabel(label);
		initializeTab();
		LOGGER.info("Tab "+ getLabel()+ " created.");
	}

	private void initializeTab() {
		addListeners();
		
	}

	private void addListeners() {
		this.addMouseListener(new GraphTabMouseListener());
		
	}

	private void setLabel(String label) {
		this.label = label==null?"":label;
	}
	
	public String getLabel(){
		return label;
	}
	
	@Override
    public void paintComponent(Graphics g) {

        super.paintComponent(g);
        doDrawing(g);
    }

	private void doDrawing(Graphics g) {
		Graphics2D g2d = (Graphics2D) g;
        
		drawVertices(g2d);
		drawPopUpsForVerticest(g2d);
		
		
	}
	
	private void drawPopUpsForVerticest(Graphics2D g2d) {
		g2d.setColor(popUpColor);
		Set<? extends Vertex> veritces = getGraph().getAllVertices();
		for(Vertex vertex: veritces){
			LinkedHashMap<String, String> infos = vertex.getInformationMap();
			Point coordinates = coordinatesMovedForVertex(vertex, infos);
			drawInformationPopUp(g2d, coordinates, infos);
		}
		LOGGER.info("Vertices Popups are draw.");
		
	}

	private Point coordinatesMovedForVertex(Vertex vertex, LinkedHashMap<String, String> infos) {
		double pupupHeight = getPopUpHeight(infos);
		double r = vertexDiameter/2.0;
		//
		
		
		double movingToCenterX = vertexDiameter/2.0;
		double movingToCenterY = -vertexDiameter/2.0;
		
		double a = r/Math.sqrt(2);
		double movingFromCentreToTheEdgeX = a;
		double movingFromCentreToTheEdgeY = a;
		
		double totalMoveX = movingToCenterX+movingFromCentreToTheEdgeX;
		double totalMoveY = movingFromCentreToTheEdgeY - pupupHeight;
		
		Point coordinates = new Point((int)(vertex.getCoordiates().getX() + totalMoveX), (int)(vertex.getCoordiates().getY()+ totalMoveY));
		
		return coordinates;
	}

	private void drawVertices(Graphics2D g2d) {
		g2d.setColor(vertexColor);
		Set<? extends Vertex> veritces = getGraph().getAllVertices();
		for(Vertex vertex: veritces){
			drawVertex(g2d, vertex);
		}
		LOGGER.info("Vertices are draw.");
	}


	private void drawVertex(Graphics2D g2d,Vertex vertex){
		Double x = vertex.getCoordiates().getX();
		Double y = vertex.getCoordiates().getY();
		Ellipse2D.Double circle = new Ellipse2D.Double(x, y, vertexDiameter, vertexDiameter);
		g2d.fill(circle);
	}
	
	
	private AbstractGraphManager getGraph(){
		return GraphsRegistryManager.getGraphManager(label);
	}
	
	private void drawInformationPopUp(Graphics2D g2d, Point coordinates, LinkedHashMap<String, String> informations){
		double popUpHeight = getPopUpHeight(informations);
		Rectangle2D.Double rectangle = new Rectangle2D.Double(coordinates.getX(), coordinates.getY(), informationsPopupWidth, popUpHeight);
		g2d.draw(rectangle);
		drawInformationInRectangle(g2d,rectangle,informations );
	}


	private void drawInformationInRectangle(Graphics2D g2d,
			Rectangle2D.Double rectangle,
			LinkedHashMap<String, String> informations) {
		double X = rectangle.getX() + informationMargin;
		double Y = rectangle.getY() + informationMargin;
		for(Entry<String,String> entry: informations.entrySet()){
			Y = Y + informationsFontSize + spaceBetweenInformations;
			String text = entry.getKey()+": "+entry.getValue();
			g2d.drawString(text, (int)X, (int)Y);
			
		}
		
	}

	private double getPopUpHeight(LinkedHashMap<String, String> informations) {
		double result = (double) (informations.size()* (informationsFontSize+spaceBetweenInformations) + 2*informationMargin);
		return result;
	}
}

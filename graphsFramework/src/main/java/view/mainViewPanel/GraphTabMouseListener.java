package view.mainViewPanel;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.logging.Logger;

import javax.swing.Timer;

import controller.ActualState;
import controller.StrategyManager;

public class GraphTabMouseListener extends MouseAdapter implements ActionListener {
	
	private final static Logger LOGGER = Logger.getLogger(StrategyManager.class.getName());
	private final static int clickInterval = (Integer)Toolkit.getDefaultToolkit().
	        getDesktopProperty("awt.multiClickInterval");

	MouseEvent lastEvent;
	Timer timer;
	
	public GraphTabMouseListener()
    {
        this(clickInterval);
    }
	
	public GraphTabMouseListener(int delay){
		timer = new Timer(delay, this);
	}
	
	public void mouseClicked (MouseEvent e)
    {
        if (e.getClickCount() > 2) return;

        lastEvent = e;

        if (timer.isRunning())
        {
            timer.stop();
            doubleClick( lastEvent );
        }
        else
        {
            timer.restart();
        }
    }
	
	public void actionPerformed(ActionEvent e)
    {
        timer.stop();
        singleClick( lastEvent );
    }
	
	public void singleClick(MouseEvent e) {
		LOGGER.info("Mouse single clicked in position "+ e.getPoint() );
		ActualState.getActualStrategy().singleClickOnActualTabAction(e);
		}
    public void doubleClick(MouseEvent e) {
    	LOGGER.info("Mouse double clicked in position "+ e.getPoint() );
    	ActualState.getActualStrategy().doubleClickOnActualTabAction(e);
    	}






}

package view.mainViewPanel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.border.LineBorder;

import controller.ApplicationStarter;

public class MainViewPanel extends JPanel {

	/**
	 * 
	 */
	
	private final static Logger LOGGER = Logger.getLogger(MainViewPanel.class.getName());
	private static Map<String, GraphTab> tabsRegistry = new HashMap<String, GraphTab>();
	
	private static final long serialVersionUID = 1L;
	private static JTabbedPane tabbedPanel;
	
	public MainViewPanel(){
		initialize();
	}

	public static void addNewTab(String title){
		if (isTabInRegistry(title)){
			LOGGER.warning("Tab " + title + " alredy exist.");
			LOGGER.warning("Tab " + title + " isn't created");
			return;
		}
		GraphTab graphTab = new GraphTab(title);
		tabbedPanel.addTab(title, graphTab);
		registerTab(title, graphTab);
		LOGGER.info("Tab " + title + " created.");
	}
	
	public static void registerTab(String title, GraphTab tab){
		tabsRegistry.put(title, tab);
	}
	
	public void removeTab(String title){
		if (!isTabInRegistry(title)){
			LOGGER.warning("Tab " + title + " don't exist.");
			return;
		}
		tabsRegistry.remove(title);
		
	}
	
	public static boolean isTabInRegistry(String title){
		return tabsRegistry.containsKey(title);
	}
	
	public static GraphTab getActiveTab(){
		if(tabbedPanel.getSelectedComponent() instanceof GraphTab){
			GraphTab actualPanel = (GraphTab) tabbedPanel.getSelectedComponent();
			LOGGER.info("Active tab is returned. ");
			return actualPanel;
		}
		LOGGER.severe("Problem with getting active Tab.");
		return null;
	}

	private void initialize() {
		setLayout(new BorderLayout(0, 0));
		tabbedPanel = new JTabbedPane();
		this.add(tabbedPanel);
	}
}

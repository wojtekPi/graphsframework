package view.mainViewPanel;

import java.awt.Component;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Logger;

import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.Popup;
import javax.swing.PopupFactory;

import configuration.ApplicationRuntimeException;
import controller.ActualState;

public class ParametersAsker extends JTextField {
	
	private final static Logger LOGGER = Logger.getLogger(ParametersAsker.class.getName());
	private String TITLE_TAB = "Please enter values.";
	private JPanel jPanel = new JPanel();
	private Component horizontalStrut = Box.createHorizontalStrut(10);
	private int DEFAULT_TEXT_FIELD_SIZE = 5;
	private LinkedHashMap<String, Class<?>> inputMap;
	private LinkedHashMap<String, JTextField> textFields = new LinkedHashMap<String, JTextField>();
	private LinkedHashMap<String, String> outputMap = new LinkedHashMap<String, String>();
	
	
	
	public ParametersAsker(Map<String, Class<?>> params){
		if(params == null){
			LOGGER.severe("Wrong input params.");
			throw new ApplicationRuntimeException("Wrong params!");
		}
		inputMap = new LinkedHashMap<String, Class<?>>(params);
		addParamsToPanel();
		showDialog();
	}

	private void addParamsToPanel() {
		for(String entry : inputMap.keySet()){
			addEntry(entry);
		}

		
	}

	private void addEntry(String entry) {
		JLabel label= new JLabel(entry);
		JTextField textField = new JTextField(DEFAULT_TEXT_FIELD_SIZE);
		jPanel.add(label);
		jPanel.add(textField);
		jPanel.add(horizontalStrut);
		textFields.put(entry, textField);
		
	}

	private void showDialog() {
		int result = JOptionPane.showConfirmDialog(ActualState.getActiveTab(), jPanel,TITLE_TAB,JOptionPane.OK_CANCEL_OPTION );
		if(result == JOptionPane.CANCEL_OPTION){
			finishAfterCancel();
		}
		else if(result == JOptionPane.OK_OPTION){
			finishAfterOk();
		}

         
		
	}

	private void finishAfterOk() {
		updateOutputMap();
		LOGGER.info("EnteredParameters are: " +outputMap);
		validateInsertedParams();
		LOGGER.info("ReturnedParameters are: " +outputMap);
		
	}

	private void updateOutputMap() {
		for(Entry<String, JTextField> entry: textFields.entrySet()){
			String key = entry.getKey();
			outputMap.put(key, entry.getValue().getText());
		}
		
	}

	private void validateInsertedParams() {
		for(Entry<String, String> entry: outputMap.entrySet()){
			String key = entry.getKey();
			String value = entry.getValue();
			Class<?> cls = inputMap.get(key);
			
			
			
			if(!isValueCorrect(cls, value)){
				LOGGER.warning("Not valid type of argument: " + key);
				LOGGER.warning("Value of " + key + " is changed to default value.");
				outputMap.put(key, getDelaultValue(cls));
			}
			
		}
		
	}

	private boolean isValueCorrect(Class<?> cls, String value) {
		if(Number.class.isAssignableFrom(cls)){
			return isNumeric(value);
		}
		return true;
		
	}

	private String getDelaultValue(Class<?> cls) {
		
		if (Number.class.isAssignableFrom(cls)) return "0";
		
		
		return "";
	}


	private void finishAfterCancel() {
		// TODO Auto-generated method stub
		
	}
	
	public Map<String, String> getValues(){
		return outputMap;
	}
	
	public static boolean isNumeric(String str)  
	{  
	  try  
	  {  
	    double d = Double.parseDouble(str);  
	  }  
	  catch(NumberFormatException nfe)  
	  {  
	    return false;  
	  }  
	  return true;  
	}

}

package view.optionsPanel;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputMethodEvent;
import java.awt.event.InputMethodListener;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.border.BevelBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import controller.StrategyManager;

public class ModeButtonsPanel extends JPanel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final String VERTEX_BUTTON_LABEL = "vertex";
	public static final String EDGE_BUTTON_LABEL = "edge";
	public static final String EDIT_BUTTON_LABEL = "edit";
	private List<JRadioButton> buttons = new ArrayList<JRadioButton>();
	private ButtonGroup group;
	
	public ModeButtonsPanel(){
		initialize();
	}

	private void initialize() {
		setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		setBorder(new BevelBorder(BevelBorder.RAISED));
		
		group = new ButtonGroup();
		createButtons();
		setInitialOptions();
		
	}

	private void setInitialOptions() {
		JRadioButton firstButton = buttons.get(0);
		firstButton.setSelected(true);
		StrategyManager.setStrategy(firstButton.getActionCommand());
		
	}

	private void createButtons() {
		
		JRadioButton vertexButton = new JRadioButton(VERTEX_BUTTON_LABEL);
		vertexButton.setMnemonic(KeyEvent.VK_V);
		vertexButton.setActionCommand(VERTEX_BUTTON_LABEL);
		
		buttons.add(vertexButton);
	    
	    JRadioButton edgeButton = new JRadioButton(EDGE_BUTTON_LABEL);
	    edgeButton.setMnemonic(KeyEvent.VK_E);
	    edgeButton.setActionCommand(EDGE_BUTTON_LABEL);
	    buttons.add(edgeButton);
	    
	    JRadioButton editButton = new JRadioButton(EDIT_BUTTON_LABEL);
	    editButton.setMnemonic(KeyEvent.VK_D);
	    editButton.setActionCommand(EDIT_BUTTON_LABEL);
	    buttons.add(editButton);
	    
	    setCommonPropertiesForButtons();
	    
	    
	    }
	
	private void setCommonPropertiesForButtons(){
		for(JRadioButton button: buttons){
	    	group.add(button);
	    	this.add(button);
	    	button.addActionListener(getListener());
	    }
	}
	
	private ActionListener getListener(){
		ActionListener listener = new ActionListener(){

			public void actionPerformed(ActionEvent e) {
				String mode = e.getActionCommand();
				StrategyManager.setStrategy(mode);

			}
			
		};
		return listener;
	}
	
}

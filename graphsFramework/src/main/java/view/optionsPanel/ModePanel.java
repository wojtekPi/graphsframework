package view.optionsPanel;

import java.awt.BorderLayout;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

public class ModePanel extends JPanel {
	
	/**
	 * 
	 */
	
	
	private static final long serialVersionUID = 1L;
	public ModePanel(){
		initialize();
	}

	private void initialize() {
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		setBorder(new TitledBorder("Mode"));
		
		createButtons();
		
	}

	private void createButtons() {
		ModeButtonsPanel modeButtonsPanel = new ModeButtonsPanel();
		add(modeButtonsPanel, BorderLayout.CENTER);
	}
}

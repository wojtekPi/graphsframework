package view.optionsPanel;

import java.awt.GridBagConstraints;

import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JToolBar;
import javax.swing.border.TitledBorder;

public class OptionsPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public OptionsPanel(){
		initialize();
	}

	private void initialize() {
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		setBorder(new TitledBorder("Options"));
		
		
		addModePanel();
		addSettingsPanel();
		
	}

	private void addSettingsPanel() {
		SettingsPanel settingsPanel = new SettingsPanel();
		add(settingsPanel);
		
	}

	private void addModePanel() {
		GridBagConstraints c = new GridBagConstraints();
		
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 0;
		c.gridy = 0;
		c.weightx = 0.5;
		
		ModePanel modePanel = new ModePanel();
		add(modePanel);
	}

	
}

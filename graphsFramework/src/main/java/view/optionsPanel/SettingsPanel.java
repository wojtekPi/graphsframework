package view.optionsPanel;

import java.awt.event.KeyEvent;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

public class SettingsPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JCheckBox autmaticCompute;
	private JButton computeButton;

	public SettingsPanel() {
		initialize();
	}

	private void initialize() {
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		setBorder(new TitledBorder("Settings"));
		
		addButtons();
	}

	private void addButtons() {
		autmaticCompute = new JCheckBox("Auto Computing");
		autmaticCompute.setMnemonic(KeyEvent.VK_C); 
		autmaticCompute.setSelected(true);
		
		add(autmaticCompute);
		
		computeButton = new JButton("Compute");
		
		add(computeButton);
		
	}
	
	
}
